# Adaptive Images
This module provides a simple umbrella catch all for your responsive images to keep you from having to curate multiple image styles and responsive image styles. It also provides a preprocess function to grab the focus point from the `focal_point` module and add it to the img tag as an inline style under the object-position property.

## Example use case
For example you may have a media item in a hero and in a 4 column teaser. Before you would potentially need two responsive image styles for each of these use cases.
One for the hero where you would resize the image to be the full width of the viewport and another for the 4 column teaser where you would resize the image to be 1/4 the width of the viewport but on tablet you maybe want the teaser to be bigger than it was on desktop.
Now you can use the same responsive image style for both and the image will adapt to the viewport size by utilizing the sizes attribute and the srcset attribute.

## Installation

Install as you would normally install a contributed Drupal module. For further

information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules).

## Recommended setup and image configuration setup.

### Configure a responsive image style
1. First go here: `/admin/config/media/responsive-image-style/add` and add a new reponsive image style called `adaptive_images`.
2. Select "Adaptive Images" as the breakpoint group.
3. In the "viewport sizings" area choose "select multiple image styles and use the sizes attribute". Note that 2x images sources should be double the width and height.
4. In the "Sizes" field enter the following: `1px`. This part isn't crazy important but will help you start small and adapt to bigger screens.
5. In the "Image styles" field select the image styles you want to use. For example: `thumbnail`, `medium`, `large`, `original`.
6. Select a "Fallback image style" if you want to use one.
7. Opt in to adaptive images by checking the "Enable adaptive images" checkbox. 
8. Save the responsive image style.

### Configure your image bundle/s for the media entity
1. Go to `/admin/structure/media/manage/image/display` (or other appropriate bundles).
2. Add and or select the view mode you want to use for the adaptive image. I recommend using the Default view mode.
3. For the image field choose the Responsive image formatter.
4. Select the `adaptive_images` responsive image style you created earlier.
5. In the settings for the responsive image formatter you can opt out of the transparent placeholder image by checking the "Opt out of transparent placeholder image" checkbox.
6. Save the settings.

### Set up template
1. IMPORTANT! Copy responsive-image--adaptive-images.html.twig from the module to your theme's templates directory.

### Block/Paragraph or other entity usage
When rendering an image for a component you can configure the field to use the view mode you enabled adaptive images on (selecting "Rendered Entity" as the display format). This will render out the img and source tags with the srcset attribute.

### Focus point
This is optional, but you can use the `focal_point` module to set a focus point on your images. This will help the adaptive image style to crop the image around the focus point using css instead of configuring a million responsive image and image style crops.

1. Follow the instructions from the `focal_point` module to enable and configure the module and the form display for the media entity.
2. See the preprocess function in this module and see that it grabs the focus point and adds the coordinates to the img tag as an inline style under the object-position property.
3. See the `adaptive-image` component css and note that there is css to handle the object-position property. Note you can use this for more than just images but iframes benefit from this too.
4. Use the adaptive image component in your theme and pass it classes to either use the aspect ratio css property or classes you provided or a framework provides like bootstrap. See https://getbootstrap.com/docs/5.0/helpers/ratio/#aspect-ratios

### Adaptive Image component
See the README in the `adaptive-image` component for usage and additional information. I recommend using this component in your theme or doing something similar where your images are using the object-fit property.
