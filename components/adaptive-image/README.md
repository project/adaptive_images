# Adaptive Image

Drop this component into your theme to use it. Opting in to adaptive images will automatically add the class "adaptive-image" around your img or picutre tags. You can use it tho manually by passing it as a class to this.

## Usage

```twig
    {{ include('adaptive_images:adaptive-image', {
      media,
      classes: ['ratio', 'ratio-16x9', 'rounded'],
    }, with_context = false) }}

    # or

    {% embed 'adaptive_images:adaptive-image' with { classes: ['ratio', 'ratio-16x9', 'rounded']} only %}
      {% block media %}
        {{ media }}
      {% endblock %}
    {% endembed %}
```

## Additional information


