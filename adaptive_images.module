<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterInterface;
function adaptive_images_theme_suggestions_responsive_image(array $suggestions) {
  $suggestions = [];
  $suggestions[] = 'responsive_image__adaptive_images';
  return $suggestions;
}

function adaptive_images_preprocess_responsive_image(array &$variables) {
  // Get the machine name of the current responsive image style.
  $responsive_image_style_id = $variables['responsive_image_style_id'];

  // Retrieve the saved value from the configuration.
  $opt_in_adaptive_images = \Drupal::config('adaptive_images.settings')->get('adpative_images_opt_in_' . $responsive_image_style_id);
  // get opt-out-transparent-placeholder from the attributes
  $opt_out_transparent_placeholder = $variables['attributes']['opt-out-transparent-placeholder'] ?? '0';
  // Check if the saved value exists and is TRUE.
  if (isset($opt_in_adaptive_images) && $opt_in_adaptive_images && $opt_out_transparent_placeholder !== '1') {
    $adaptive_images_path = \Drupal::service('extension.list.module')->getPath('adaptive_images');
    $placeholder = $adaptive_images_path . '/img/p.png';
    /** @var \Drupal\Core\Template\Attribute $source */
    foreach($variables['sources'] as $key => $source) {
      // turn string into attributes
      /** @var \Drupal\Core\Template\AttributeString $srcset */
      $srcset = $source['srcset'];
      // get the string
      $srcset = $srcset->__toString();
      // get the image in the img directory of this module called placeholder.png
      $srcset = '/' . $placeholder . ' 1w, ' . $srcset;

      $source->setAttribute('srcset', $srcset);
      $variables['sources'][$key] = $source;
    }

    // Add the placeholder image to the image tag
    $variables['img_element']['#uri'] = $placeholder;
    // Add the placeholder image to the image tag srcset
    $variables['img_element']['#attributes']['srcset'] = $placeholder . ' 1w, ' . $variables['img_element']['#attributes']['srcset'];
  }

  if (isset($opt_in_adaptive_images) && $opt_in_adaptive_images) {
    // Create a new attribute object.
    $attributes = new Attribute(['class' => ['adaptive-image']]);
    // Add the attribute object to the variables array.
    $variables['wrapper_attributes'] = $attributes;
    // attach the library
    $variables['#attached']['library'][] = 'adaptive_images/adaptive_images';
  }
}

/**
 * Implements template_preprocess_media().
 */
function adaptive_images_preprocess_responsive_image_formatter(array &$variables) {
  // https://www.drupal.org/project/media_library_media_modify could use this to have differnt points but would have to change code below.
  /** @var \Drupal\media\Entity\Media $media */
  $media = $variables['item']->getEntity();
  if ($media instanceof \Drupal\media\Entity\Media) {
    $mediaBundle = $media->bundle();
    if ($mediaBundle === 'image') {
      $image_factory = \Drupal::service('image.factory');
      $has_file = !$media->get("field_media_image")->isEmpty();
      if ($has_file) {
        /** @var \Drupal\file\FileInterface $file */
        $file = $media->get("field_media_image")->entity;
        $crop = \Drupal::service('focal_point.manager')->getCropEntity($file, 'focal_point');
        $image_info = $image_factory->get($file->getFileUri());
        $image_width = $image_info->getWidth();
        $image_height = $image_info->getHeight();
        if (isset($image_width) && isset($image_height)) {
          if ($crop->position()) {
            $focalTemp = $crop->position();
            if ($focalTemp) {
              $focal_x = (int) round($focalTemp['x'] / $image_width * 100);
              $focal_y = (int) round($focalTemp['y'] / $image_height * 100);
              $focalPoint = $focal_x . '% ' . $focal_y . '%';
              $focalPointPlain = $focal_x . '-' . $focal_y;
              $variables['responsive_image']['#attributes']['style'][] = "object-position: " . $focalPoint . ';';
              $variables['responsive_image']['#attributes']['class'][] = "object-position--" . $focalPointPlain;
            }
          }
        }
      }
    }
  }
}

/**
 * Implements template_preprocess_field().
 */
function adaptive_images_preprocess_field(&$variables) {
  $element = $variables['element'];
  if (
    !empty($element['#third_party_settings'])
    && !empty($element['#third_party_settings']['adaptive_images']['opt_out_transparent_placeholder'])
  ) {
    foreach ($variables['items'] as $key => $item) {
      // I want oto pass the third party setting down to the image formatter
      $variables['items'][$key]['content']['#item_attributes']['opt-out-transparent-placeholder'] = $element['#third_party_settings']['adaptive_images']['opt_out_transparent_placeholder'];
    }
  }
}


/**
 * Implements hook_form_FORM_ID_alter() for responsive image style form.
 */
function adaptive_images_form_responsive_image_style_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  // Get the machine name of the current responsive image style.
  $responsive_image_style_id = $form_state->getFormObject()->getEntity()->id();

  // Retrieve the saved value from the configuration.
  $saved_value = \Drupal::config('adaptive_images.settings')->get('adpative_images_opt_in_' . $responsive_image_style_id);

  // Add a third-party checkbox to the form.
  $form['adpative_images_opt_in_' . $responsive_image_style_id] = [
    '#type' => 'checkbox',
    '#title' => t('Opt In to Adaptive Images'),
    '#description' => t('Check this box to opt in to using adaptive images.'),
    // Set the default value to the saved value if it exists, otherwise set it to FALSE.
    '#default_value' => isset($saved_value) ? $saved_value : FALSE,
  ];

  // Add a custom submit handler to the form.
  $form['actions']['submit']['#submit'][] = 'adaptive_images_form_responsive_image_style_form_submit';
}

/**
 * Custom submit handler for the responsive image style form.
 */
function adaptive_images_form_responsive_image_style_form_submit($form, \Drupal\Core\Form\FormStateInterface $form_state) {
  // Get the machine name of the current responsive image style.
  $responsive_image_style_id = $form_state->getFormObject()->getEntity()->id();

  // Save the new value of the third-party checkbox to the configuration.
  \Drupal::configFactory()->getEditable('adaptive_images.settings')->set('adpative_images_opt_in_' . $responsive_image_style_id, $form_state->getValue('adpative_images_opt_in_' . $responsive_image_style_id))->save();
}

function adaptive_images_field_formatter_third_party_settings_form(FormatterInterface $plugin, FieldDefinitionInterface $field_definition, $view_mode, array $form, FormStateInterface $form_state) {
  // test if the selected responsive image style is the one we want to add the checkbox to.
  $element = [];
  $allowed_formatter_types = ['lazy_responsive_image', 'responsive_image'];
  if (in_array($plugin->getPluginId(), $allowed_formatter_types)){
    $element['opt_out_transparent_placeholder'] = [
      '#type' => 'checkbox',
      '#title' => t('Opt Out of Transparent Placeholder'),
      '#description' => t('Check this box to opt out of the transparent placeholder.'),
      '#default_value' => $plugin->getThirdPartySetting('adaptive_images', 'opt_out_transparent_placeholder', FALSE),
    ];
  }

  return $element;
}

function adaptive_images_field_formatter_settings_submit($form, FormStateInterface $form_state) {
  $plugin = $form_state->getFormObject()->getEntity();
  if ($plugin->getPluginId() === 'responsive_image') {
    $plugin->setThirdPartySetting('adaptive_images', 'opt_out_transparent_placeholder', $form_state->getValue('opt_out_transparent_placeholder'));
  }
}
