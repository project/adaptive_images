(function (Drupal) {
  'use strict';

  Drupal.behaviors.adaptiveImageWrappers = {
    images: [],
    attach: adaptiveImageWrappersizes,
  };

  function adaptiveImageWrappersizes(context) {
    const adaptiveImageWrappers = context.querySelectorAll('.adaptive-image');

    // Collect the images.
    adaptiveImageWrappers.forEach((adaptiveImageWrapper) => {
      this.images.push(adaptiveImageWrapper);
    });

    // Create a resize observer to update the image sizes.
    const [body] = once('adaptive-image-wrapper', 'body', context);
    if (body) {
      new ResizeObserver(Drupal.debounce(() => {
        this.images.forEach((adaptiveImageWrapper) => {
          imageSizes(adaptiveImageWrapper);
        });
      }, 250)).observe(body);
      // Create an intersection observer to detect visibility changes.
      const observer = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            imageSizes(entry.target);
          }
        });
      }, { threshold: [0] });

      this.images.forEach((adaptiveImageWrapper) => {
        observer.observe(adaptiveImageWrapper);
      });
    }

  }

  // Function to set image sizes
  function imageSizes(adaptiveImageWrapper) {
    const image = adaptiveImageWrapper.querySelector('img');

    if (image) {
      let imageWidth = Math.floor(adaptiveImageWrapper.getBoundingClientRect().width) ?? '100vw';
      const allImages = [image, ...adaptiveImageWrapper.querySelectorAll('img, source')];
      allImages.forEach((el) => {
        el.setAttribute('sizes', `${imageWidth}`);
        el.setAttribute('data-sizes', `${imageWidth}`);
      });
    }
  }

})(Drupal);
